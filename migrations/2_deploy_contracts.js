var BHAX = artifacts.require("./bhax/BhaxToken.sol");

module.exports = function(deployer) {
  var equityInstance;

  deployer.then(function() {
    return deployer.deploy(BHAX);
  }).then(function(instance) {
    return BHAX.deployed();
  }).then(function(contractObject) {
    equityInstance = contractObject;
    console.log('>>> BHAX: ', equityInstance.address);
  });
};
