# Smart Contract of Bhax Token

Website: [https://bithashex.com/](https://bithashex.com/)

Facebook: [https://www.facebook.com/bithashex](https://www.facebook.com/bithashex)

Twitter: [https://twitter.com/bithashex](https://twitter.com/bithashex)

Medium: [https://bithashex.medium.com/](https://bithashex.medium.com/)

Quora: [https://www.quora.com/profile/Bithashex/](https://www.quora.com/profile/Bithashex/)

Pinterest: [https://www.pinterest.com/bithashex/](https://www.pinterest.com/bithashex/)

Coinmarketcap: [https://coinmarketcap.com/currencies/bithashex/](https://coinmarketcap.com/currencies/bithashex/)

Coinranking: [https://coinranking.com/coin/wLJzjZMpG+bithashex-bhax/](https://coinranking.com/coin/wLJzjZMpG+bithashex-bhax/)
